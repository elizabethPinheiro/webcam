
import React from 'react';
//import Switch from '@material-ui/core/Switch';
import Webcam from 'react-webcam';
//import Button from '@material-ui/core/Button';
//import CameraIcon from '@material-ui/icons/Camera';
//import CropIcon from '@material-ui/icons/Crop';
//import UndoIcon from '@material-ui/icons/Undo';
//import EditIcon from '@material-ui/icons/Edit';
//import SaveIcon from '@material-ui/icons/Save';
//import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Croppie from 'croppie';


const downloadjs = require('downloadjs');

export default class WebcamCapture extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      camIsOn: true,
      imageData: null,
      saved: false,
    };
  }

  setRef = (webcam) => {
    this.webcam = webcam;
  };

  capture = () => {
    const imageSrc = this.webcam.getScreenshot();
    this.setState({ imageData: imageSrc });
  };

  crop = () => {
    const el = document.getElementById('demo-basic');
    console.log(el);
    const vanilla = new Croppie(el, {
      viewport: { width: 120, height: 161, type: 'square' },
      boundary: { width: 300, height: 300 },
      showZoomer: true,
      enableOrientation: false,
      enableResize: false,
      mouseWheelZoom:false,
    });
    vanilla.bind({
      url: this.state.imageData,
      orientation: 1,
    });


    const doCropBtn = document.getElementById('doCrop');
    const saveBtn = document.getElementById('saveBtn');
    
    doCropBtn.style.display = 'inline-flex';
    console.log(' WebcamCapture -> crop -> doCropBtn', doCropBtn);
    doCropBtn.addEventListener('click', () => {
      vanilla.result({ type: 'blob', format: 'jpeg', size: 'original' })
        .then((blob) => {
          document.getElementById('imgblob').setAttribute('src', blob);
          this.setState({ imageData: blob });
          doCropBtn.style.display = 'none';
          saveBtn.style.display = 'inline-flex';
        });
    });
  }

  render() {
    const videoConstraints = {
      width: 500,
      height: 480,
     
    };

    const webcam = (
      <div id="webcam">
        <Webcam
          audio={false}
          width={400} // 640
          height={480} // 480
          ref={this.setRef}
          screenshotFormat="image/jpeg"
          videoConstraints={videoConstraints}
        />
      </div>
    );

    const photo = (
      <div id="still-photo">
        <img id="photo" alt="" src={this.state.imageData} />
      </div>
    );

    const campic = this.state.imageData === null ? webcam : photo;
    const buttons = this.state.imageData === null ? (
      <button onClick={this.capture} variant="contained" color="primary">
      
        CAPTURE
      </button>
    )
      : (
        <button
          onClick={() => this.setState({ imageData: null })}
          variant="contained"
          color="primary"
        >
        
         Retake
        </button>
      );

    const doCropBtn = (
      <button
        style={{ display: 'none' }}
        id="doCrop"
        variant="contained"
        color="primary"
      >
      
        Crop Selection
      </button>
    );

    const saveBtn = (
      <button
        style={{ display: 'none' }}
        onClick={() => {
          downloadjs(
            this.state.imageData, prompt('Nome do arquivo'), 'image/jpeg',
          ); this.setState({ saved: true });
        }}
        id="saveBtn"
        variant="contained"
        color="primary"
      >
        
        Salvar
      </button>
    );

    const cloudUploadBtn = (
      <button
        onClick={() => this.setState({ imageData: null, camIsOn: false, saved: false })}
        variant="contained"
        color="secondary"
        size="large"
      >
    
        Upload & Reset
      </button>
    );

    return (
      <>
        <h3>1. CAPTURE</h3>
        <p id="webcam-text">
              Camera
        </p>
        {this.state.camIsOn && campic}
        {this.state.camIsOn && buttons}

        {this.state.imageData && (
          <>
            <h3>2. CROP</h3>
            <button
              onClick={() => this.crop()}
              variant="contained"
              color="primary"
            >
         
              Crop Photo...
            </button>
            <div id="demo-basic" />

            <img alt="" id="imgblob" />

            {doCropBtn}

            {saveBtn}

           


          </>
        )}

      </>
    );
  }
}
