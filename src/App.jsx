import React, { Component } from 'react';
import './App.css';
import WebcamCapture from './Components/WebcamCapture';

class App extends  Component {
  constructor(props) {
    super(props);

    this.state = {
      // camIsOn: false,
    };
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>
            <br />Webfoto
          </h1>
          <h2>Captura Foto</h2>
          <p>
            <br />
          (1) Capture
          <br />
          (2) Crop
          <br />
          </p>
          <WebcamCapture />
        </header>
      </div>
    );
  }
}
export default App;
